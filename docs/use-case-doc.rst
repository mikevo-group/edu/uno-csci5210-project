Use Cases
=========

The following use cases have been outlined:

.. toctree::
   :maxdepth: 2

   use_cases/UC1
   use_cases/UC2
   use_cases/UC3
   use_cases/UC4
   use_cases/UC5
   use_cases/UC6
   use_cases/UC7
   use_cases/UC8
   use_cases/UC9

.. image:: diagrams/use_case.png
